// Copyright 2019 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
import 'package:flutter/material.dart';
import 'src/basics/animationHW.dart';


void main() => runApp(AnimationSamples());
class Item {
  final String name;
  final String route;
  final WidgetBuilder builder;
  const Item({this.name, this.route, this.builder});
}
final pages = [
  Item(
      name: 'Fade, Color, Icon Animation Change',
      route: FadeTransitionDemo.routeName,
      builder: (context) => FadeTransitionDemo()),
];

final routes = Map.fromEntries(pages.map((d) => MapEntry(d.route, d.builder)));

final allRoutes = <String, WidgetBuilder>{
  ...routes,
};
class AnimationSamples extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Animation Selection',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      routes: allRoutes,
      home: HomePage(),
    );
  }
}
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final headerStyle = Theme.of(context).textTheme.headline6;
    return Scaffold(
      appBar: AppBar(
        title: Text('Navigation'),
      ),
      body: ListView(
        children: [
          ListTile(title: Text('Select your route below', style: headerStyle)),
          ...pages.map((d) => Selection(d)),
        ],
      ),
    );
  }
}
class Selection extends StatelessWidget {
  final Item item;
  Selection(this.item);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(item.name),
      onTap: () {
        Navigator.pushNamed(context, item.route);
      },
    );
  }
}
