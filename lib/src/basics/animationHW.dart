import 'package:flutter/material.dart';
import 'dart:math';

Color generateColor() => Color(0xFFFFFFFF & Random().nextInt(0xFFFFFFFF));

IconData generateIcon() {
  Random random = new Random();
  final iconCollection = [Icons.favorite, Icons.audiotrack, Icons.beach_access, Icons.ac_unit, Icons.bug_report];
  return iconCollection[random.nextInt(5)];
}

class FadeTransitionDemo extends StatefulWidget {
  static const String routeName = '/animation';

  @override
  _FadeTransitionDemoState createState() => _FadeTransitionDemoState();

}

class _FadeTransitionDemoState extends State<FadeTransitionDemo> with SingleTickerProviderStateMixin {
  Color color;
  IconData icon;

  AnimationController _controller;
  Animation<double> _animation;
  CurvedAnimation _curve;

  @override
  void initState() {
    super.initState();
    color = Colors.deepPurple;

    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 2000))
    ..addStatusListener((status) {
      if(status == AnimationStatus.completed){
        _controller.reverse();
        change();
      }
      else if (status == AnimationStatus.dismissed){
        _controller.forward();
      }
    });
    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    _animation = Tween(
      begin: 1.0,
      end: 0.0,
    ).animate(_curve);

    _controller.forward();
  }
  void change() {
    setState(() {
      color = generateColor();
      icon = generateIcon();
    });
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Animated Widget',
        ),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            FadeTransition(
              opacity: _animation,
              child: Icon(
                icon,
                color: color,
                size: 300,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
